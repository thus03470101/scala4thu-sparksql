import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")  //看左邊的打

  src.createOrReplaceTempView("ratings")    //"raings"為table name

  //1.show:把結果SELECT * FROM ratings）列出來
  ss.sql("SELECT * FROM ratings").show()

  //2.根據timestamp做降序排列
  ss.sql("SELECT * FROM ratings ORDER BY timestamp DESC").show()

  //3.計算每部電影的平均rating, show出來的格式須為|movieId|mean|
  ss.sql("SELECT movieId,AVG(rating) as mean FROM ratings GROUP BY movieId").show()

  //4.格式為|movieId|max|min|
  ss.sql("SELECT movieId,max(rating) as max,min(rating) as min FROM ratings GROUP BY movieId").show()

  //5.找出movieId為30707且userId=107799，格式為|userId|movieId|rating| timestamp|
  ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799").show()

 //6.從movie.csv中，找出movieId為10的資料 ，格式為|movieId|title|
  val src1: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")
  src1.createOrReplaceTempView("movies")

  ss.sql("SELECT movieId,title FROM movies WHERE movieId=10").show()

  //7.從small-ratings.csv及movies.csv兩張表Join起來，以timestamp降序排列，格式為|movieId|title|rating|timestamp|  !!!刪掉group by 

  ss.sql("SELECT r.movieId,m.title,r.rating,r.timestamp FROM ratings as r JOIN movies AS m WHERE m.movieId=r.movieId ORDER BY timestamp DESC").show()

}




///view -> tool windows -> database -> + -> data source -> sqlite -> ...找資料
  //-> downloads -> test
